<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class TasksPrueba extends Controller{
    
    public function GetTasks(){
        // $tasks = Task::orderBy('created_at', 'asc')->get();
        $tasks = DB::table('tasks')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->select('users.id', 'users.name', 'tasks.id', 'tasks.name')
            ->get();

        return view('tasks', ['tasks' => $tasks]);
   }
}
